<?php

class ServiceController extends Zend_Controller_Action
{
	public function sparqlAction()
	{
		// disable view render and layout for ajax request
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout()->disableLayout();
		
		$store = Mmdb::getInstance()->erfurt->getStore();
		$response = $this->getResponse();

		$queryString = $this->_request->getParam('query', '');
		
		if (!empty($queryString)) {
			//$query = Erfurt_Sparql_SimpleQuery::initWithString($queryString);
			$store->_backendAdapter->sparqlQuery($queryString, $options);
		}
	}
}
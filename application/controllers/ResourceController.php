<?php

class ResourceController extends Zend_Controller_Action
{
	public function viewAction()
	{
		$this->view->resource = $this->_request->_requestUri;
		$this->view->testa = $this->view->serverUrl();
		$this->view->headScript()->appendFile('/js/' . 'handlebars-helper.js');
		$this->view->headScript()->appendFile('/js/' . 'rdfmod.js');
		$this->view->headScript()->appendFile('/js/' . 'mmdb.js');
		$this->view->headLink()->appendStylesheet('/css/' . 'resource.css');
	}
}
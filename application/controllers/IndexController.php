<?php

class IndexController extends Zend_Controller_Action
{

    public function init()
    {
        /* Initialize action controller here */
    }

    public function indexAction()
    {
        // action body
        $this->view->abc = 'def';
        $this->view->headScript()->appendFile('/js/' . 'mmdb.js');
        
        $bbcode = Zend_Markup::factory('Textile');
        echo $bbcode->render('[b]bold text[/b] and [i]cursive text[/i]');
    }
		
    public function aboutAction()
    {
    	$abc = new stdClass();
    	$abc->title = 'title';
    	$abc->content = 'content';
    	$this->view->data = array($abc);
    }
}


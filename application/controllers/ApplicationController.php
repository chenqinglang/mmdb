<?php

/**
 * Mmdb application controller.
 *
 * @package OntoWiki_Controller
 * @author Norman Heino <norman.heino@gmail.com>
 * @author Philipp Frischmuth <pfrischmuth@googlemail.com>
 */
class ApplicationController extends Zend_Controller_Action {
	function searchAction() {
		$this->view->placeholder('main.window.title')->set($title);
	}
}
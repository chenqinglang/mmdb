<?php

class Bootstrap extends Zend_Application_Bootstrap_Bootstrap
{
	public function _initConfig()
	{
		$config = new Zend_Config_Ini(APPLICATION_PATH . 'configs/default.ini', 'default', true);
		try {
			$privateConfig = new Zend_Config_Ini(MMDB_ROOT . 'config.ini', 'private', true);
			$config->merge($privateConfig);
		} catch (Zend_Config_Exception $e) {
			
		}
		return $config;
	}
	
	public function _initRouter()
	{
		// require front controller
		$this->bootstrap('frontController');
		$frontController = $this->getResource('frontController');
		
		$this->bootstrap('Config');
		$config = $this->getResource('Config');
		
		$router = $frontController->getRouter();
		$router->addConfig($config->routes);
		
		return $router;
	}
	
	/**
	 * Initializes the Erfurt framework
	 *
	 */
	public function _initErfurt()
	{
		$erfurt = null;
	
		// require Config
		$this->bootstrap('Config');
		$config = $this->getResource('Config');
	
		// require OntoWiki
		$this->bootstrap('Mmdb');
		$ontoWiki = $this->getResource('Mmdb');
	
		// require Logger, since Erfurt logger should write into OW logs dir
		//$this->bootstrap('Logger');
	
		// Reset the Erfurt app for testability... needs to be refactored.
		Erfurt_App::reset();
	
		try {
			$erfurt = Erfurt_App::getInstance(false)->start($config);
		} catch (Erfurt_Exception $ee) {
			//throw new OntoWiki_Exception('Error loading Erfurt framework: ' . $ee->getMessage());
		} catch (Exception $e) {
			//throw new OntoWiki_Exception('Unexpected error: ' . $e->getMessage());
		}
	
		// make available
		$Mmdb->erfurt = $erfurt;
	
		return $erfurt;
	}
	
	/**
	 * Initializes the OntoWiki main class
	 *
	 */
	public function _initMmdb()
	{
		// require Config
		$this->bootstrap('Config');
		$config = $this->getResource('Config');
	
		Mmdb::reset();
		$Mmdb = Mmdb::getInstance();
		$Mmdb->setBootstrap($this);
		$Mmdb->language = isset($config->languages->locale) ? $config->languages->locale : null;
		$Mmdb->config   = $config;
	
		return $Mmdb;
	}
	
	/**
	 * Initializes the session and loads session variables
	 *
	 */
	public function _initSession()
	{
		// require Config
		$this->bootstrap('Config');
		$config = $this->getResource('Config');
	
		// require Config
		$this->bootstrap('Mmdb');
		$Mmdb = $this->getResource('Mmdb');
	
		// init session
		$sessionKey = 'ONTOWIKI' . (isset($config->session->identifier) ? $config->session->identifier : '');
		$session    = new Zend_Session_Namespace($sessionKey);
	
		// define the session key as a constant for global reference
	
		if ( false === defined('_OWSESSION'))
			define('_OWSESSION', $sessionKey);
	
		// inject session vars into OntoWiki
		if (array_key_exists('sessionVars', $this->_options['bootstrap'])) {
			$Mmdb->setSessionVars((array)$this->_options['bootstrap']['sessionVars']);
		}
	
		// make available
		$Mmdb->session = $session;
	
		return $session;
	}
}


<?php

class Mmdb
{
	protected $_bootstrap = null;
	protected $_properties = array();
	protected static $_instance = null;
	
	/**
	 * Variables to be autoloaded from the session
	 * @var array
	 */
	protected $_sessionVars = array();
	
	/**
	 * Disallow cloning
	 */
	private function __clone()
	{
		
	}
	
	public static function reset()
	{
		self::$_instance = null;
	}
	
	/**
	 * Singleton instance
	 *
	 * @return OntoWiki
	 */
	public static function getInstance()
	{
		if (null === self::$_instance) {
			self::$_instance = new self();
		}
	
		return self::$_instance;
	}
	
	public function setBootstrap($bootstrap)
	{
		$this->_bootstrap = $bootstrap;
	}
	
	/**
	 * Returns a property value
	 *
	 * @param string $propertyName
	 * @return mixed
	 * @since 0.9.5
	 */
	public function __get($propertyName)
	{
		// retrieve from session
		if (in_array($propertyName, $this->_sessionVars)) {
			if (isset($this->session->$propertyName)) {
				$this->_properties[$propertyName] = $this->session->$propertyName;
			}
		}
	
		// retrieve bootstrap resource
		$bootstrap = $this->getBootstrap();
		if ($bootstrap and $bootstrap->hasResource($propertyName)) {
			return $bootstrap->getResource($propertyName);
		}
	
		// retrieve locally
		if (isset($this->$propertyName)) {
			return $this->_properties[$propertyName];
		}
	}
	
	/**
	 * Sets a property
	 *
	 * @param string $propertyName
	 * @param mixed $propertyValue
	 */
	public function __set($propertyName, $propertyValue)
	{
		// set in session
		if (in_array($propertyName, $this->_sessionVars)) {
			$this->session->$propertyName = $propertyValue;
		}
	
		// set locally
		$this->_properties[$propertyName] = $propertyValue;
	}

	/**
	 * Returns the application bootstrap object
	 *
	 */
	public function getBootstrap()
	{
		if (null === $this->_bootstrap) {
			$frontController  = Zend_Controller_Front::getInstance();
			$this->_bootstrap = $frontController->getParam('bootstrap');
		}
	
		return $this->_bootstrap;
	}
}
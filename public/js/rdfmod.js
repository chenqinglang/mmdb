
var RdfMod = {
	ns : {
		rdf : 'http://www.w3.org/1999/02/22-rdf-syntax-ns#',
		rdfs : 'http://www.w3.org/2000/01/rdf-schema#',
		owl : 'http://www.w3.org/2002/07/owl#',
		foaf: 'http://xmlns.com/foaf/0.1/'
	},
	
	sparqlEndpoint : 'http://localhost:3030/data/sparql',
	
	init : function() {alert('rdfmod');},
	
	addns : function(ns, content) {
		this.ns[ns] = content;
	},
	
	loadproperty: function(uri, graphuri, success) {
		var queryString = this.addprefix() + "select ?s ?p ?o \
							from  <" + graphuri + ">  \
							where {" + "<" + uri + "> ?p ?o.} ";
		return this.query(queryString, success);
	},
	
	addprefix: function() {
		var prefix = '';
		for(name in this.ns) {
			prefix = prefix + " PREFIX " + name + ": <" + this.ns[name]+ "> ";
		}
		return prefix;
	},
	
	query: function(queryString, success) {
		$.ajax({
			type: "POST",
			url: this.sparqlEndpoint,
			data: {query: queryString, output:'json'},
			dataType: 'json',
			success: function(data) {
				var nsdata = RdfMod.matchns(data);
				success(nsdata);
			},
			error: function(data, err1, err2) {
				alert(err2);
			}
		});
	},
	
	// Match the result with predefined ns
	matchns: function(data) {
		data.results.bindings.forEach(function(x){
			for(prop in x) {
				if (!x.hasOwnProperty(prop)) continue;
				for(ns in RdfMod.ns) {
					if (x[prop].value.match(RdfMod.ns[ns])) {
						x[prop].value = ns + ":" + x[prop].value.substr(RdfMod.ns[ns].length);
						break;
					}
				}
			}
		});
		return data;
	}
};

// add music ontology to RdfMod as 'mo'
RdfMod.addns('mo', 'http://purl.org/ontology/mo/');
RdfMod.addns('mmdb', 'http://www.mmdb.cn/');
RdfMod.addns('mmdb-ont', 'http://www.mmdb.cn/ontology/');
RdfMod.addns('mmdb-res', 'http://www.mmdb.cn/resource/');

// Make Model based on current Resource URL
var Resource = Backbone.Model.extend({
	defaults: {
		graphuri : "http://www.mmdb.cn/test",
		id : window.location.href,
	},
	initialize: function() {
	},
	
	// Parse json query result
	parseJson: function(model, resp, options) {
		resp.results.bindings.forEach(function(x){
			if(model.has(x.p.value)) {
				model.get(x.p.value).push({type : x.o.type , value : x.o.value});					
			} 
			else {
				model.set(x.p.value , [{type : x.o.type , value : x.o.value}], {silent: true});
			};
		});
		model.change();
	},
	
});

Resource.prototype.sync = function(method, model, options) {
	switch (method) {
		case "read": RdfMod.loadproperty(model.id, model.get('graphuri'), options.success); 
	}
};

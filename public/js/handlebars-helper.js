Handlebars.registerHelper('link', function(object, key) {
	if (key in object) {
		return object[key][0].value;
	}
});
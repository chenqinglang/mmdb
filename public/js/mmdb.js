// Good

$(document).ready(function() {
	
	var someResource = new Resource;
	
	window.AppView = Backbone.View.extend({
		el: $('#resourcepage'),

		events: {},
		initialize: function() {
			//_.bindAll(this, 'render', );
			
			someResource.fetch({success: someResource.parseJson});
			var view = new ResourceView({model: someResource});
			this.$("#resource").html(view.render().el);
		}
	});
	
	window.ResourceView = Backbone.View.extend({
		tagName: "div",
		className: "resource",
		template: $("#resourceproperty").html(),
		initialize: function() {
			_.bindAll(this, 'render');
			this.model.bind('change', this.render);
		},
		
		render: function() {
			var template = Handlebars.compile(this.template);
			context = this.model.toJSON();
			wrapper = {context: context};
			var html = template(wrapper);
			$(this.el).html(html);
			return this;
		}
	});
	
	window.App = new AppView;
});



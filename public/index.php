<?php

/**
 * Mmdb bootstrap file
 */

/**
 * Bootstrap constants
 */
define('BOOTSTRAP_FILE', basename(__FILE__));
define('MMDB_ROOT', realpath(dirname(__FILE__) . '/../') . DIRECTORY_SEPARATOR);
define('APPLICATION_PATH', MMDB_ROOT . 'application'. DIRECTORY_SEPARATOR);
define('CACHE_PATH', MMDB_ROOT . 'cache'. DIRECTORY_SEPARATOR);


// Define path to application directory
// defined('APPLICATION_PATH')
//     || define('APPLICATION_PATH', realpath(dirname(__FILE__) . '/../application'));

// Define application environment
defined('APPLICATION_ENV')
    || define('APPLICATION_ENV', (getenv('APPLICATION_ENV') ? getenv('APPLICATION_ENV') : 'development'));

// Ensure libraries is on include_path
set_include_path(implode(PATH_SEPARATOR, array(
    MMDB_ROOT . 'libraries',
    MMDB_ROOT . 'libraries/Erfurt/library',
    get_include_path(),
)));


define('MMDB_REWRITE', TRUE);

/** Zend_Application */
require_once 'Zend/Application.php';

// Create application, bootstrap, and run
$application = new Zend_Application(
    APPLICATION_ENV,
    APPLICATION_PATH . 'configs/application.ini'
);

/** check/include OntoWiki */
try {
	// use include, so we can catch it with the error handler
	require_once 'Mmdb.php';
} catch (Exception $e) {
	header('HTTP/1.1 500 Internal Server Error');
	echo 'Fatal Error: Could not load the OntoWiki Application Framework classes.<br />' . PHP_EOL
	. 'Your installation directory seems to be screwed.';
	return;
}

/* check/include Erfurt_App */
try {
	// use include, so we can catch it with the error handler
	require_once 'Erfurt/App.php';
} catch (Exception $e) {
	header('HTTP/1.1 500 Internal Server Error');
	echo 'Fatal Error: Could not load the Erfurt Framework classes.<br />' . PHP_EOL
	. 'Maybe you should install it with apt-get or with "make deploy"?';
	return;
}

$application->bootstrap()
            ->run();